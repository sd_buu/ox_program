/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.ox_program;

import java.util.Scanner;

/**
 *
 * @author domem
 */
public class OX_program {
    public static void main(String[] args) {
        Scanner ip = new Scanner(System.in);
        char [][] table = {{'1', '2', '3'},
                         {'4', '5', '6'},
                         {'7', '8', '9'}};
        System.out.println("Welcome to OX-Game");
        System.out.println(table[0][0]+ " | " + table[0][1]+ " | " + table[0][2]);
        System.out.println("--+---+--");
        System.out.println(table[1][0]+ " | " + table[1][1]+ " | " + table[1][2]);
        System.out.println("--+---+--");
        System.out.println(table[2][0]+ " | " + table[2][1]+ " | " + table[2][2]);
        Boolean end = false;
        int turn = 0;
        String winner = "Draw";
        while (end != true){
            if (turn == 9 && end == false){
                System.out.println();
                break;
            }
            if (table[0][0] == 'O' && table[0][1] == 'O' && table[0][2] == 'O' 
                || table[1][0] == 'O' && table[1][1] == 'O' && table[1][2] == 'O'
                || table[2][0] == 'O' && table[2][1] == 'O' && table[2][2] == 'O'
                
                || table[0][0] == 'O' && table[1][0] == 'O' && table[2][0] == 'O'
                || table[0][1] == 'O' && table[1][1] == 'O' && table[2][1] == 'O'
                || table[0][2] == 'O' && table[1][2] == 'O' && table[2][2] == 'O'
                
                || table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O'
                || table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O'){
                winner = "O";
                break;
            }
            if (table[0][0] == 'X' && table[0][1] == 'X' && table[0][2] == 'X' 
                || table[1][0] == 'X' && table[1][1] == 'X' && table[1][2] == 'X'
                || table[2][0] == 'X' && table[2][1] == 'X' && table[2][2] == 'X'
                
                || table[0][0] == 'X' && table[1][0] == 'X' && table[2][0] == 'X'
                || table[0][1] == 'X' && table[1][1] == 'X' && table[2][1] == 'X'
                || table[0][2] == 'X' && table[1][2] == 'X' && table[2][2] == 'X'
                
                || table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X'
                || table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X'){
                winner = "X";
                break;
            }
            if (turn%2==0){
                System.out.println("Turn O: ");
                System.out.println("input number 1-9: ");
                int num = ip.nextInt();
                if (num == 1){
                    table[0][0] = 'O';
                }else if (num == 2){
                    table[0][1] = 'O';
                }else if (num == 3){
                    table[0][2] = 'O';
                }else if (num == 4){
                    table[1][0] = 'O';
                }else if (num == 5){
                    table[1][1] = 'O';
                }else if (num == 6){
                    table[1][2] = 'O';
                }else if (num == 7){
                    table[2][0] = 'O';
                }else if (num == 8){
                    table[2][1] = 'O';
                }else if (num == 9){
                    table[2][2] = 'O';
                }
                    System.out.println(table[0][0]+ " | " + table[0][1]+ " | " + table[0][2]);
                    System.out.println("--+---+--");
                    System.out.println(table[1][0]+ " | " + table[1][1]+ " | " + table[1][2]);
                    System.out.println("--+---+--");
                    System.out.println(table[2][0]+ " | " + table[2][1]+ " | " + table[2][2]);
            }else{
                System.out.println("Turn X: ");
                System.out.println("input number 1-9: ");
                int num = ip.nextInt();
                if (num == 1){
                    table[0][0] = 'X';
                }else if (num == 2){
                    table[0][1] = 'X';
                }else if (num == 3){
                    table[0][2] = 'X';
                }else if (num == 4){
                    table[1][0] = 'X';
                }else if (num == 5){
                    table[1][1] = 'X';
                }else if (num == 6){
                    table[1][2] = 'X';
                }else if (num == 7){
                    table[2][0] = 'X';
                }else if (num == 8){
                    table[2][1] = 'X';
                }else if (num == 9){
                    table[2][2] = 'X';
                }
                    System.out.println(table[0][0]+ " | " + table[0][1]+ " | " + table[0][2]);
                    System.out.println("--+---+--");
                    System.out.println(table[1][0]+ " | " + table[1][1]+ " | " + table[1][2]);
                    System.out.println("--+---+--");
                    System.out.println(table[2][0]+ " | " + table[2][1]+ " | " + table[2][2]);
            }
            turn++;
        }
        if (turn == 9){
            System.out.println(">>Draw!<<");
        }else{
            System.out.println(">>"+winner +"  WIN!"+"<<");
        }
        
    }
}
